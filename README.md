# README #

Necessário Android Studio e SDK da API 15 do Android.

### Para que foi criado esse repositório? ###

Projeto Final da disciplina Programação para Dispositivos Móveis, ministrada na Universidade Federal do ABC.

### Como configurar? ###

* Baixar todo o projeto
* Abrir o Android Studio
* Abrir um projeto existente
* Escolher a pasta desse projeto

### Com quem conversar? ###

* Ricardo, Thomaz ou Tiago