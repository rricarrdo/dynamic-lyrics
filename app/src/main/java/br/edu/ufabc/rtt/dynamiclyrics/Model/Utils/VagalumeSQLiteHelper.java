package br.edu.ufabc.rtt.dynamiclyrics.Model.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;

import br.edu.ufabc.rtt.dynamiclyrics.Model.Playlist;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeResult;

/**
 * Created by Thz on 17/04/2015.
 */
public class VagalumeSQLiteHelper extends SQLiteOpenHelper {
    private static final String LOGTAG = VagalumeSQLiteHelper.class.getSimpleName();

    public static final String TABLE_VAGALUME_RESULTS = "vagalume_results";
    public static final String TABLE_PLAYLIST = "playlist";
    public static final String TABLE_VAGALUME_RESULTS_PLAYLIST = "vagalume_results_playlist";

    /* Colunas da tabela Vagalume Results */
    public static final String COLUMN_ARTIST_ID = "artist_id";
    public static final String COLUMN_ARTIST_NAME = "artist_name";
    public static final String COLUMN_ARTIST_URL = "artist_url";

    public static final String COLUMN_MUSIC_ID = "music_id"; // Utilizado também como foreign key de VagalumeResults_Playlist
    public static final String COLUMN_MUSIC_NAME = "music_name";
    public static final String COLUMN_MUSIC_URL = "music_url";
    public static final String COLUMN_MUSIC_LANG = "music_lang";
    public static final String COLUMN_MUSIC_TEXT = "music_text";

    public static final String COLUMN_MUSIC_TRANSLATE_TEXT = "music_translate_text";

    public static final String COLUMN_YOUTUBE_ID = "youtube_id";

    public static final String COLUMN_ACCESS_DATE = "access_date";

    public static final String COLUMN_FAVORITE = "favorite";

    /* Colunas da tabela Playlist */
    public static final String COLUMN_PLAYLIST_ID = "playlist_id"; // Utilizado também como foreign key de VagalumeResults_Playlist
    public static final String COLUMN_PLAYLIST_NAME = "playlist_name";

    public static final String COLUMN_VAGALUME_RESULT_PLAYLIST_ID = "vagalume_result_playlist_id";

    private String[] vagalume_result_columns = {COLUMN_MUSIC_ID, COLUMN_MUSIC_NAME,
            COLUMN_MUSIC_URL, COLUMN_MUSIC_LANG, COLUMN_MUSIC_TEXT,
            COLUMN_MUSIC_TRANSLATE_TEXT, COLUMN_ARTIST_ID, COLUMN_ARTIST_NAME,
            COLUMN_ARTIST_URL, COLUMN_YOUTUBE_ID, COLUMN_ACCESS_DATE, COLUMN_FAVORITE};

    private String[] playlist_columns = {COLUMN_PLAYLIST_ID, COLUMN_PLAYLIST_NAME};

    private String[] vagalume_result_playlist_columns = {COLUMN_VAGALUME_RESULT_PLAYLIST_ID, COLUMN_MUSIC_ID, COLUMN_PLAYLIST_ID};

    private static final String DB_NAME = "vagalume_results.db";
    private static final int DB_VERSION = 2;

    private SQLiteDatabase db;

    private static final String CREATE_TABLE_VAGALUME_RESULT = "CREATE TABLE " + TABLE_VAGALUME_RESULTS + "("
            + COLUMN_MUSIC_ID + " TEXT PRIMARY KEY,"
            + COLUMN_MUSIC_NAME + " TEXT NOT NULL,"
            + COLUMN_MUSIC_URL + " TEXT,"
            + COLUMN_MUSIC_LANG + " TEXT,"
            + COLUMN_MUSIC_TEXT + " TEXT NOT NULL,"
            + COLUMN_MUSIC_TRANSLATE_TEXT + " TEXT NULL,"
            + COLUMN_ARTIST_ID + " TEXT NOT NULL,"
            + COLUMN_ARTIST_NAME + " TEXT NOT NULL,"
            + COLUMN_ARTIST_URL + " TEXT,"
            + COLUMN_YOUTUBE_ID + " TEXT NULL,"
            + COLUMN_ACCESS_DATE + " INTEGER NOT NULL,"
            + COLUMN_FAVORITE + " INTEGER NOT NULL"
            + ");";

    private static final String CREATE_TABLE_PLAYLIST = "CREATE TABLE " + TABLE_PLAYLIST + "("
            + COLUMN_PLAYLIST_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_PLAYLIST_NAME + " TEXT NOT NULL"
            + ");";

    private static final String CREATE_TABLE_VAGALUME_RESULT_PLAYLIST = "CREATE TABLE " + TABLE_VAGALUME_RESULTS_PLAYLIST + "("
            + COLUMN_VAGALUME_RESULT_PLAYLIST_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_MUSIC_ID + " INTEGER,"
            + COLUMN_PLAYLIST_ID + " INTEGER"
            + ");";

    private static final String DROP_TABLE_VAGALUME_RESULT = "DROP TABLE IF EXISTS " + TABLE_VAGALUME_RESULTS;
    private static final String DROP_TABLE_PLAYLIST = "DROP TABLE IF EXISTS " + TABLE_PLAYLIST;
    private static final String DROP_TABLE_VAGALUME_RESULT_PLAYLIST = "DROP TABLE IF EXISTS " + TABLE_VAGALUME_RESULTS_PLAYLIST;

    public VagalumeSQLiteHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE_VAGALUME_RESULT);
            db.execSQL(CREATE_TABLE_PLAYLIST);
            db.execSQL(CREATE_TABLE_VAGALUME_RESULT_PLAYLIST);
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to create database", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL(DROP_TABLE_VAGALUME_RESULT);
            db.execSQL(DROP_TABLE_PLAYLIST);
            db.execSQL(DROP_TABLE_VAGALUME_RESULT_PLAYLIST);
        } catch (SQLiteException e) {
            Log.e(LOGTAG, "Failed to drop the databse", e);
        }
        onCreate(db);
    }

    public void insertVagalumeResult(VagalumeResult result) {
        ContentValues values = new ContentValues();

        Date accessDate = new Date();

        values.put(COLUMN_MUSIC_ID, result.getMusicId());
        values.put(COLUMN_MUSIC_NAME, result.getMusicName());
        values.put(COLUMN_MUSIC_URL, result.getMusicUrl());
        values.put(COLUMN_MUSIC_LANG, result.getMusicLang());
        values.put(COLUMN_MUSIC_TEXT, result.getMusicText());
        values.put(COLUMN_MUSIC_TRANSLATE_TEXT, result.getTranslateText());
        values.put(COLUMN_ARTIST_ID, result.getArtistId());
        values.put(COLUMN_ARTIST_NAME, result.getArtistName());
        values.put(COLUMN_ARTIST_URL, result.getArtistUrl());
        values.put(COLUMN_YOUTUBE_ID, result.getYoutubeID());
        values.put(COLUMN_ACCESS_DATE, accessDate.getTime());
        values.put(COLUMN_FAVORITE, result.isFavorite() ? 1 : 0);

        db.insertWithOnConflict(TABLE_VAGALUME_RESULTS, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void updateAccessTime(VagalumeResult result) {
        ContentValues values = new ContentValues();
        Date newAcessDate = new Date();

        values.put(COLUMN_MUSIC_ID, result.getMusicId());
        values.put(COLUMN_MUSIC_NAME, result.getMusicName());
        values.put(COLUMN_MUSIC_URL, result.getMusicUrl());
        values.put(COLUMN_MUSIC_LANG, result.getMusicLang());
        values.put(COLUMN_MUSIC_TEXT, result.getMusicText());
        values.put(COLUMN_MUSIC_TRANSLATE_TEXT, result.getTranslateText());
        values.put(COLUMN_ARTIST_ID, result.getArtistId());
        values.put(COLUMN_ARTIST_NAME, result.getArtistName());
        values.put(COLUMN_ARTIST_URL, result.getArtistUrl());
        values.put(COLUMN_YOUTUBE_ID, result.getYoutubeID());
        values.put(COLUMN_ACCESS_DATE, newAcessDate.getTime());
        values.put(COLUMN_FAVORITE, result.isFavorite() ? 1 : 0);

        String where = COLUMN_MUSIC_ID + " = ?";
        String[] arguments = {result.getMusicId()};

        db.update(TABLE_VAGALUME_RESULTS, values, where, arguments);
    }

    public ArrayList<VagalumeResult> getMostRecents() {
        ArrayList<VagalumeResult> results = new ArrayList<>();

        Cursor cursor = null;
        String orderBy = COLUMN_ACCESS_DATE + " DESC";
        cursor = db.query(TABLE_VAGALUME_RESULTS, vagalume_result_columns, null, null, null, null, orderBy, "10");
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                VagalumeResult result = mapearCursorCompletoParaVagalumeResult(cursor);
//                Log.d("SQLiteHelper", result.toString());
                results.add(result);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return results;
    }

    public long insertPlaylist(Playlist playlist){
        ContentValues values = new ContentValues();

        values.put(COLUMN_PLAYLIST_NAME, playlist.getName());

        long id = db.insert(TABLE_PLAYLIST, null, values);

        return id;
    }

    public long removePlaylist(Playlist playlist){
        String query = COLUMN_PLAYLIST_ID + "=" + playlist.getId();

        long id = db.delete(TABLE_PLAYLIST, query , null);
        db.delete(TABLE_VAGALUME_RESULTS_PLAYLIST, query, null);

        return id;
    }

    public long insertVagalumeResultPlaylist(VagalumeResult result, Playlist playlist){
        ContentValues values = new ContentValues();

        values.put(COLUMN_MUSIC_ID, result.getMusicId());
        values.put(COLUMN_PLAYLIST_ID, playlist.getId());

        Log.d(LOGTAG, "Tentando inserir " + result.toString() + " na playlist " + playlist.toString());

        long id = db.insert(TABLE_VAGALUME_RESULTS_PLAYLIST, null, values);

        return id;
    }

    public long removeVagalumeResultPlaylist(VagalumeResult result, Playlist playlist){
        String query = COLUMN_PLAYLIST_ID + " = \'" + playlist.getId() + "\' and " + COLUMN_MUSIC_ID + " = \'" + result.getMusicId()+"\'";

        long id = db.delete(TABLE_VAGALUME_RESULTS_PLAYLIST, query, null);

        return id;
    }

    public int getMusicsCountByPlaylist(Playlist playlist){
        int count = 0;

        String rawQuery = "SELECT COUNT(*) FROM " + TABLE_VAGALUME_RESULTS_PLAYLIST + " WHERE " + COLUMN_PLAYLIST_ID + " = ?";
        String[] arguments = {String.valueOf(playlist.getId())};

        Cursor cursor = db.rawQuery(rawQuery, arguments);
        if(cursor.moveToFirst())
            count = cursor.getInt(0);

        cursor.close();
        return count;
    }

    public Playlist getPlaylistByName(String playlistName){

        Playlist playlist = new Playlist();
        Cursor cursor = null;

        String where = COLUMN_PLAYLIST_NAME + " = ?";
        String[] arguments = {playlistName};
        cursor = db.query(TABLE_PLAYLIST, playlist_columns, where, arguments, null, null, null, "1");

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                playlist = mapearCursorCompletoParaPlaylist(cursor);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return playlist;
    }

    public ArrayList<Playlist> getAllPlaylists(){
        ArrayList<Playlist> playlists = new ArrayList<>();
        String orderBy = COLUMN_PLAYLIST_NAME + " ASC";

        Cursor cursor = null;
        cursor = db.query(TABLE_PLAYLIST, playlist_columns, null, null, null, null, orderBy, null);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Playlist playlist = mapearCursorCompletoParaPlaylist(cursor);
                Log.d(LOGTAG, playlist.toString());
                playlists.add(playlist);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return playlists;
    }

    public ArrayList<VagalumeResult> getVagalumeResultsByPlaylist(Playlist playlist){
        ArrayList<VagalumeResult> results = new ArrayList<>();

        String rawQuery = "SELECT * FROM " + TABLE_VAGALUME_RESULTS + " VR, "
                + TABLE_PLAYLIST + " PL, " + TABLE_VAGALUME_RESULTS_PLAYLIST + " VR_PL WHERE PL."
                + COLUMN_PLAYLIST_ID + " = ? AND PL." + COLUMN_PLAYLIST_ID + " = VR_PL."
                + COLUMN_PLAYLIST_ID + " AND VR." + COLUMN_MUSIC_ID + " = VR_PL." + COLUMN_MUSIC_ID;

        String[] arguments = {String.valueOf(playlist.getId())};

        Cursor cursor = null;
        cursor = db.rawQuery(rawQuery, arguments);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                VagalumeResult result = mapearCursorCompletoParaVagalumeResult(cursor);
                results.add(result);
                cursor.moveToNext();
            }
        }
        cursor.close();

        return results;
    }

    public VagalumeResult getMusicOnPlaylist(Playlist playlist, VagalumeResult vagalumeResult ){
        VagalumeResult resultFound = null;
        ArrayList<VagalumeResult> results = new ArrayList<>();

        String rawQuery = "SELECT * FROM " + TABLE_VAGALUME_RESULTS + " VR, "
                + TABLE_PLAYLIST + " PL, " + TABLE_VAGALUME_RESULTS_PLAYLIST + " VR_PL WHERE PL."
                + COLUMN_PLAYLIST_ID + " = ? AND PL." + COLUMN_PLAYLIST_ID + " = VR_PL."
                + COLUMN_PLAYLIST_ID + " AND VR." + COLUMN_MUSIC_ID + " = VR_PL." + COLUMN_MUSIC_ID;

        String[] arguments = {String.valueOf(playlist.getId())};

        Cursor cursor = null;
        cursor = db.rawQuery(rawQuery, arguments);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                VagalumeResult result = mapearCursorCompletoParaVagalumeResult(cursor);

                if(result.getArtistName().equals(vagalumeResult.getArtistName()) &&
                        result.getMusicName().equals(vagalumeResult.getMusicName())){
                    resultFound = result;
                }

                results.add(result);
                cursor.moveToNext();
            }
        }
        cursor.close();

        return resultFound;
    }

    public ArrayList<VagalumeResult> getFavorites() {
        ArrayList<VagalumeResult> results = new ArrayList<>();

        String where = COLUMN_FAVORITE + " = ?";
        String[] arguments = {"1"}; // 1 == true
        String orderBy = COLUMN_ACCESS_DATE + " DESC";

        Cursor cursor = null;
        cursor = db.query(TABLE_VAGALUME_RESULTS, vagalume_result_columns, where, arguments, null, null, orderBy, null);
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                VagalumeResult result = mapearCursorCompletoParaVagalumeResult(cursor);
                results.add(result);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return results;
    }

    public VagalumeResult getMusic(String musicId) {

        VagalumeResult vagalumeResult = new VagalumeResult();
        Cursor cursor = null;

        String where = COLUMN_MUSIC_ID + " = ?";
        String[] arguments = {musicId};
        cursor = db.query(TABLE_VAGALUME_RESULTS, vagalume_result_columns, where, arguments, null, null, null, "1");

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                vagalumeResult = mapearCursorCompletoParaVagalumeResult(cursor);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return vagalumeResult;
    }

    private VagalumeResult mapearCursorCompletoParaVagalumeResult(Cursor cursor) {
        VagalumeResult result = new VagalumeResult();

        result.setMusicId(cursor.getString((cursor.getColumnIndex(COLUMN_MUSIC_ID))));
        result.setMusicName(cursor.getString((cursor.getColumnIndex(COLUMN_MUSIC_NAME))));
        result.setMusicUrl(cursor.getString((cursor.getColumnIndex(COLUMN_MUSIC_URL))));
        result.setMusicLang(cursor.getString((cursor.getColumnIndex(COLUMN_MUSIC_LANG))));
        result.setMusicText(cursor.getString((cursor.getColumnIndex(COLUMN_MUSIC_TEXT))));
        result.setTranslateText(cursor.getString((cursor.getColumnIndex(COLUMN_MUSIC_TRANSLATE_TEXT))));
        result.setArtistId(cursor.getString((cursor.getColumnIndex(COLUMN_ARTIST_ID))));
        result.setArtistName(cursor.getString((cursor.getColumnIndex(COLUMN_ARTIST_NAME))));
        result.setArtistUrl(cursor.getString((cursor.getColumnIndex(COLUMN_ARTIST_URL))));
        result.setYoutubeID(cursor.getString((cursor.getColumnIndex(COLUMN_YOUTUBE_ID))));
        result.setAccessDate(cursor.getLong((cursor.getColumnIndex(COLUMN_ACCESS_DATE))));
        result.setFavorite(cursor.getInt((cursor.getColumnIndex(COLUMN_FAVORITE))) == 1 ? true : false);

        return result;
    }

    private Playlist mapearCursorCompletoParaPlaylist(Cursor cursor) {
        Playlist playlist = new Playlist();

        playlist.setId(cursor.getInt((cursor.getColumnIndex(COLUMN_PLAYLIST_ID))));
        playlist.setName(cursor.getString((cursor.getColumnIndex(COLUMN_PLAYLIST_NAME))));

        return playlist;
    }
}
