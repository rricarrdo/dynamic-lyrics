package br.edu.ufabc.rtt.dynamiclyrics.Controller;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import br.edu.ufabc.rtt.dynamiclyrics.Model.Playlist;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeDAO;
import br.edu.ufabc.rtt.dynamiclyrics.R;

/**
 * Created by Thz on 01/05/2015.
 */
public class PlaylistAdapter extends BaseAdapter{
    private ArrayList<Playlist> playlists;

    private LayoutInflater inflater;
    private Context context;
    private static final String LOGTAG = "PlaylistAdapter";
    public PlaylistAdapter(Context context){
        this.context = context;
        playlists = VagalumeDAO.newInstance(context).getAllPlaylists();
        if(playlists == null)
            Log.e(LOGTAG, "playlist nula");
        if(playlists.size() == 0)
            Log.e(LOGTAG, "playlist vazia");
    }

    @Override
    public int getCount() {
        return playlists.size();
    }

    @Override
    public Object getItem(int position) {
        return playlists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View playlistRow = convertView;
        PlaylistHolder holder = null;

        if(playlistRow == null){
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            playlistRow = inflater.inflate(R.layout.playlist_row, parent, false);
            holder = new PlaylistHolder(playlistRow);
            playlistRow.setTag(holder);
        }else{
            holder = (PlaylistHolder) playlistRow.getTag();
        }

        Playlist playlist = playlists.get(position);

        holder.playlistNameTextView.setText(playlist.getName());

        String musicCount = VagalumeDAO.newInstance(context).getMusicsCountByPlaylist(playlist) + " " + context.getString(R.string.music_count);
        holder.numMusicsTextView.setText(musicCount);

        return playlistRow;
    }

    public void updateContent(){
        playlists.clear();
        playlists.addAll(VagalumeDAO.newInstance(context).getAllPlaylists());
    }

    private class PlaylistHolder{
        TextView playlistNameTextView;
        TextView numMusicsTextView;

        PlaylistHolder(View view){
            playlistNameTextView = (TextView) view.findViewById(R.id.tvPlaylistName);
            numMusicsTextView = (TextView) view.findViewById(R.id.tvNumMusics);
        }
    }
}
