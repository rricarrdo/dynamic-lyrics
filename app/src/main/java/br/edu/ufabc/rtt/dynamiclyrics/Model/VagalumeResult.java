package br.edu.ufabc.rtt.dynamiclyrics.Model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ricardo on 06/04/2015.
 */
public class VagalumeResult implements Serializable{

    private String type;

    private String artistId;
    private String artistName;
    private String artistUrl;

    private String musicId;
    private String musicName;
    private String musicUrl;
    private String musicLang;
    private String musicText;

    private String translateId;
    private String translateLang;
    private String translateUrl;
    private String translateText;

    private String youtubeID;

    private Date accessDate;

    private boolean favorite;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getArtistUrl() {
        return artistUrl;
    }

    public void setArtistUrl(String artistUrl) {
        this.artistUrl = artistUrl;
    }

    public String getMusicId() {
        return musicId;
    }

    public void setMusicId(String musicId) {
        this.musicId = musicId;
    }

    public String getMusicName() {
        return musicName;
    }

    public void setMusicName(String musicName) {
        this.musicName = musicName;
    }

    public String getMusicUrl() {
        return musicUrl;
    }

    public void setMusicUrl(String musicUrl) {
        this.musicUrl = musicUrl;
    }

    public String getMusicLang() {
        return musicLang;
    }

    public void setMusicLang(String musicLang) {
        this.musicLang = musicLang;
    }

    public String getMusicText() {
        return musicText;
    }

    public void setMusicText(String musicText) {
        this.musicText = musicText;
    }

    public String getTranslateId() {
        return translateId;
    }

    public void setTranslateId(String translateId) {
        this.translateId = translateId;
    }

    public String getTranslateLang() {
        return translateLang;
    }

    public void setTranslateLang(String translateLang) {
        this.translateLang = translateLang;
    }

    public String getTranslateUrl() {
        return translateUrl;
    }

    public void setTranslateUrl(String translateUrl) {
        this.translateUrl = translateUrl;
    }

    public String getTranslateText() {
        return translateText;
    }

    public void setTranslateText(String translateText) {
        this.translateText = translateText;
    }

    public String getYoutubeID() {
        return youtubeID;
    }

    public void setYoutubeID(String youtubeID) {
        this.youtubeID = youtubeID;
    }

    public Date getAccessDate() {
        return accessDate;
    }

    public void setAccessDate(Long time) {
        this.accessDate = new Date(time);
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @Override
    public String toString() {
        return "VagalumeResult{" +
                "artistName='" + artistName + '\'' +
                ", musicName='" + musicName + '\'' +
                ", favorite=" + favorite +
                '}';
    }
}
