package br.edu.ufabc.rtt.dynamiclyrics.Controller.Services;

        import android.app.IntentService;
        import android.content.Intent;
        import android.support.v4.content.LocalBroadcastManager;

        import br.edu.ufabc.rtt.dynamiclyrics.Controller.Vagalume_Fetcher;
        import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeResult;


public class VagalumeFetcher_Service extends IntentService {
    public static final String PARAMS_NOME_ARTISTA = "nomeArtista";
    public static final String PARAMS_NOME_MUSICA = "nomeMusica";
    public static final String PARAM_VAGALUME_RESULT = "vagalumeResult";
    public static final String BROADCAST_ACTION = VagalumeFetcher_Service.class.getPackage() + ".DOWNLOADCOMPLETE";

    private Vagalume_Fetcher fetcher;

    public VagalumeFetcher_Service() {
        super("VagalumeFetcher_Service");
        fetcher = new Vagalume_Fetcher();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String nomeArtista = intent.getStringExtra(PARAMS_NOME_ARTISTA);
        String nomeMusica = intent.getStringExtra(PARAMS_NOME_MUSICA);
        VagalumeResult vagalumeResult = fetcher.fetch(nomeArtista, nomeMusica);
        Intent localIntent;

        localIntent = new Intent(BROADCAST_ACTION);
        localIntent.putExtra(PARAM_VAGALUME_RESULT, vagalumeResult);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }
}