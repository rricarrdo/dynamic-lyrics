package br.edu.ufabc.rtt.dynamiclyrics.View.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import br.edu.ufabc.rtt.dynamiclyrics.Controller.VagalumeResultAdapter;
import br.edu.ufabc.rtt.dynamiclyrics.Model.Playlist;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeDAO;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeResult;
import br.edu.ufabc.rtt.dynamiclyrics.R;
import br.edu.ufabc.rtt.dynamiclyrics.View.MainActivity;


public class LyricsListFragment extends ListFragment {
    private IComportamentoFragments callback;
    private VagalumeResultAdapter adapter;
    private TextView titleTextView;
    private Playlist playlist;
    private int mode;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Qaundo o fragment eh associado na activity, podemos recuperar a Interface para fazer a comunicacao
        callback = (IComportamentoFragments) activity;
        playlist = null;
        mode = getArguments().getInt(MainActivity.LIST_MODE);

        if (mode == MainActivity.FAVORITES_MODE)
            adapter = new VagalumeResultAdapter(getActivity(), MainActivity.FAVORITES_MODE, null);
        if (mode == MainActivity.PLAYLIST_MODE) {
            playlist = (Playlist) getArguments().getSerializable("Playlist");
//            Log.d("LyricsList", "playlist passada com sucesso " + playlist.toString());
            adapter = new VagalumeResultAdapter(getActivity(), MainActivity.PLAYLIST_MODE, playlist);
        }

        setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favorites, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        titleTextView = (TextView) getActivity().findViewById(R.id.tvLyricsListTitle);
        if (mode == MainActivity.FAVORITES_MODE)
            titleTextView.setText(getString(R.string.tvFavorites));
        else if (mode == MainActivity.PLAYLIST_MODE)
            titleTextView.setText(getString(R.string.tvPlaylist) + " - " + playlist.getName());

        registerForContextMenu(getListView());
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (mode == MainActivity.FAVORITES_MODE) {
            getActivity().getMenuInflater().inflate(R.menu.menu_list_context_favoritos, menu);
        } else if (mode == MainActivity.PLAYLIST_MODE) {
            getActivity().getMenuInflater().inflate(R.menu.menu_list_context_playlists_lyrics, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        int id = item.getItemId();

        if (id == R.id.action_remove) {

            if (mode == MainActivity.FAVORITES_MODE) {
                VagalumeDAO dao = VagalumeDAO.newInstance(getActivity());
                VagalumeResult favorito = (VagalumeResult) getListAdapter().getItem(info.position);
                favorito.setFavorite(false);
                dao.addVagalumeResult(favorito);

                adapter = new VagalumeResultAdapter(getActivity(), MainActivity.FAVORITES_MODE, null);
                setListAdapter(adapter);
                getListView().invalidateViews();
            } else if (mode == MainActivity.PLAYLIST_MODE) {
                VagalumeDAO dao = VagalumeDAO.newInstance(getActivity());
                VagalumeResult playlistMusica = (VagalumeResult) getListAdapter().getItem(info.position);

                dao.removeVagalumeResultPlaylist(playlistMusica, playlist);
                adapter = new VagalumeResultAdapter(getActivity(), MainActivity.PLAYLIST_MODE, playlist);
                setListAdapter(adapter);
                getListView().invalidateViews();
            }

        }
        return super.onContextItemSelected(item);
    }


    public void onResume() {
        super.onResume();

        if (mode == MainActivity.FAVORITES_MODE) {
            adapter = new VagalumeResultAdapter(getActivity(), MainActivity.FAVORITES_MODE, null);
        } else if (mode == MainActivity.PLAYLIST_MODE) {
            adapter = new VagalumeResultAdapter(getActivity(), MainActivity.PLAYLIST_MODE, playlist);
        }

        setListAdapter(adapter);
        getListView().invalidateViews();
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        VagalumeResult result = (VagalumeResult) adapter.getItem(position);
        VagalumeDAO.newInstance(getActivity()).addVagalumeResult(result);

        adapter.notifyDataSetInvalidated();
        getListView().invalidate();
        getListView().invalidateViews();
        callback.musicaArmazenada(result);
    }

}
