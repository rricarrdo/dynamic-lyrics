package br.edu.ufabc.rtt.dynamiclyrics.View.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.EditText;

import br.edu.ufabc.rtt.dynamiclyrics.Controller.PlaylistAdapter;
import br.edu.ufabc.rtt.dynamiclyrics.Model.Playlist;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeDAO;
import br.edu.ufabc.rtt.dynamiclyrics.R;

/**
 * Created by Thz on 01/05/2015.
 */
public class InsertPlaylistDialog extends DialogFragment {

    private DialogDismissal callback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            callback = (DialogDismissal) getTargetFragment();
        } catch (ClassCastException e) {

            Log.w("InsertPlaylistDialog", e);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //LayoutInflater inflater = getActivity().getLayoutInflater();
        final EditText input = new EditText(getActivity());
        input.setHint(getString(R.string.playlistName));

        builder//.setView(inflater.inflate(R.layout.dialog_playlist_insert, null))
                .setView(input)
                .setTitle(R.string.new_playlist)
                .setPositiveButton(R.string.create, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String playlistName = input.getText().toString().trim();
                        if (playlistName != null && !playlistName.equals("")) {
                            Playlist playlist = new Playlist();
                            playlist.setName(playlistName);
                            VagalumeDAO.newInstance(getActivity()).addPlaylist(playlist);
                            try {
                                callback.onDialogDismissal();
                            } catch (Exception e) {
                                Log.w("InsertPlaylistDialog", e);
                            }
                        } else
                            Log.e("InsertPlaylistDialog", "Erro ao adicionar playlist");


                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        return builder.create();
    }

    public interface DialogDismissal {
        void onDialogDismissal();
    }

}

