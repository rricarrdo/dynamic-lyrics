package br.edu.ufabc.rtt.dynamiclyrics.Model;

import java.io.Serializable;

/**
 * Created by Thz on 01/05/2015.
 */
public class Playlist implements Serializable{
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Playlist{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
