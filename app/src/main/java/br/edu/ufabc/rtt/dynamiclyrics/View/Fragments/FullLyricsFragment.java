package br.edu.ufabc.rtt.dynamiclyrics.View.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeDAO;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeResult;
import br.edu.ufabc.rtt.dynamiclyrics.R;

/**
 * Created by Ricardo on 17/04/15.
 */
public class FullLyricsFragment extends Fragment {

    public String idMusica = null;
    private VagalumeDAO dao;
    private Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        setRetainInstance(true);
        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_full_lyrics, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        activity = getActivity();
        dao = VagalumeDAO.newInstance(getActivity());

        //Recupera a música do Banco de Dados
        if (idMusica != null) {
            VagalumeResult musica = dao.buscarMusica(idMusica);

            TextView tvTitulo = (TextView) activity.findViewById(R.id.tvTitulo);
            if (musica != null) {
                TextView tvLetraCompletaMusica = (TextView) activity.findViewById(R.id.tvLetraCompletaMusica);

                tvTitulo.setText(musica.getMusicName() + " - " + musica.getArtistName());
                tvLetraCompletaMusica.setText(musica.getMusicText());
            } else {
                tvTitulo.setText(activity.getString(R.string.ERRO_LETRA_COMPLETA));
            }
        }
    }

}
