package br.edu.ufabc.rtt.dynamiclyrics.View.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import java.util.ArrayList;
import java.util.Scanner;

import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeDAO;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeResult;
import br.edu.ufabc.rtt.dynamiclyrics.R;
import br.edu.ufabc.rtt.dynamiclyrics.View.SettingsActivity;

/**
 * Created by Ricardo on 08/04/2015.
 */
public class LyricsFragment extends Fragment {
    private static final int LINE_NUMBERS_DEFAULT = 3;
    private static final int FONT_SIZE_DEFAULT = 15;

    private static final boolean VIDEO_ENABLED_DEFAULT = true;

    private SharedPreferences sharedPref;

    private boolean alreadyLoaded = false;

    private Thread updateLyricsThread;

    private TextView tvNomeMusica;
    private TextView tvLetra;
    private SeekBar sbVelocidadeLetras;
    private Button pauseButton;
    private Button voltarLetra, avancarLetra;
    private FrameLayout flYoutube;
    YouTubePlayerSupportFragment youTubePlayerFragment;

    private VagalumeResult vagalumeResult;
    private ArrayList<String> formatedLyrics;
    private int lineNumbers;
    private int fontSize;
    private String fontColor;
    private float swapSpeed = 5.0f;
    private boolean pauseLyric = false;
    private boolean updateLyric = false;
    private int lyricPivot = 0;
    private boolean videoEnabled;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_lyrics, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
//        menu.clear();
        inflater.inflate(R.menu.menu_lyrics, menu);

        if (vagalumeResult.isFavorite()) {
            menu.findItem(R.id.action_favorite_lyric).setIcon(android.R.drawable.btn_star_big_on);
        }

        if(vagalumeResult.getTranslateText() == null){
            menu.findItem(R.id.action_open_translation).setVisible(false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        PauseLyrics();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_full_lyrics:
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();

                FullLyricsFragment fullLyricsFragment = new FullLyricsFragment();
                //atribui o id da musica para ser recuperada pelo fragment de Full Lyrics
                fullLyricsFragment.idMusica = vagalumeResult.getMusicId();

                //Tenta buscar o fragment_unico -> Layout Portrait
                View fragmentUnico = getActivity().findViewById(R.id.fragment_unico);

                if (fragmentUnico != null) {
                    transaction.replace(R.id.fragment_unico, fullLyricsFragment);
                } else {
                    transaction.replace(R.id.fragment_direita, fullLyricsFragment);
                }
                transaction.addToBackStack(null);
                transaction.commit();
                return true;
            case R.id.action_playlist_dialog:
                DialogFragment playlistsDialog = new ShowPlaylistsDialog();
                Bundle args = new Bundle();
                args.putSerializable("VagalumeResult", vagalumeResult);
                playlistsDialog.setArguments(args);
                playlistsDialog.show(getActivity().getSupportFragmentManager(), "ShowPlaylistsDialog");
                return true;
            case R.id.action_settings:
                startActivity(new Intent(this.getActivity(), SettingsActivity.class));
                PauseLyrics();
                return true;
            case R.id.action_favorite_lyric:
                if (vagalumeResult.isFavorite()) {
                    item.setIcon(android.R.drawable.btn_star_big_off);
                } else {
                    item.setIcon(android.R.drawable.btn_star_big_on);
                }
                favoriteLyric();
                return true;
            case R.id.action_open_translation:

                new AlertDialog.Builder(getActivity())
                        .setTitle(vagalumeResult.getMusicName())
                        .setMessage(vagalumeResult.getTranslateText())
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_menu_mapmode)
                        .show();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onResume() {
        super.onResume();
        recuperarViews();
        atualizaPreferencias();
        if (videoEnabled && getActivity() != null && vagalumeResult.getYoutubeID() != null) {
            flYoutube.setVisibility(View.VISIBLE);
            inicializarYoutube(vagalumeResult.getYoutubeID());
        } else {
            flYoutube.setVisibility(View.GONE);
        }
        PauseLyrics();
    }


    @Override
    public void onStart() {
        super.onStart();

        recuperarViews();
        preencherCamposTela(vagalumeResult);

    }

    private void recuperarViews() {
        tvNomeMusica = (TextView) getActivity().findViewById(R.id.tvNomeMusica);
        tvLetra = (TextView) getActivity().findViewById(R.id.tvLetra);
        sbVelocidadeLetras = (SeekBar) getActivity().findViewById(R.id.sbVelocidadeLetras);
        pauseButton = (Button) getActivity().findViewById(R.id.pauseButton);
        flYoutube = (FrameLayout) getActivity().findViewById(R.id.fl_yotube);
        voltarLetra = (Button) getActivity().findViewById(R.id.voltarLetra);
        avancarLetra = (Button) getActivity().findViewById(R.id.avancarLetra);

        atribuirComportamentos();
    }

    private void atualizaPreferencias() {
        lineNumbers = sharedPref.getInt(getString(R.string.num_lines_key), LINE_NUMBERS_DEFAULT);
        fontSize = sharedPref.getInt(getString(R.string.font_size_key), FONT_SIZE_DEFAULT);
        fontColor = sharedPref.getString(getString(R.string.font_colors_key), getString(R.string.font_color_default));
        videoEnabled = sharedPref.getBoolean(getString(R.string.video_enable_key), VIDEO_ENABLED_DEFAULT);

        if (videoEnabled && vagalumeResult.getYoutubeID() != null) {
            flYoutube.setVisibility(View.VISIBLE);
        } else {
            flYoutube.setVisibility(View.GONE);
        }

        tvLetra.setTextSize(fontSize);
        tvLetra.setTextColor(Color.parseColor(fontColor));
    }

    private void atribuirComportamentos() {

        sbVelocidadeLetras.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                swapSpeed = (10.0f - (float) seekBar.getProgress() / 10) / 2;
//                Toast.makeText(getActivity(), "Valor: " + ((float) seekBar.getProgress() / 10), Toast.LENGTH_SHORT).show();
                updateLyric = true;
            }
        });

        pauseButton.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                int id = v.getId();

                switch (id) {
                    case R.id.pauseButton:
                        if (pauseLyric) {
                            UnpauseLyrics();
                        } else
                            PauseLyrics();
                        break;
                    default:
                        Toast.makeText(v.getContext(), "Botão Desconhecido...", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });

        voltarLetra.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                int id = v.getId();

                switch (id) {
                    case R.id.voltarLetra:
                        if (lyricPivot > 1) {
                            lyricPivot -= 2;
                            updateLyric = true;
                        }
                        break;
                    default:
                        Toast.makeText(v.getContext(), "Botão Desconhecido...", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });

        avancarLetra.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                int id = v.getId();

                switch (id) {
                    case R.id.avancarLetra:
                        if (lyricPivot < formatedLyrics.size() - lineNumbers) {
                            lyricPivot++;
                            updateLyric = true;
                        }
                        break;
                    default:
                        Toast.makeText(v.getContext(), "Botão Desconhecido...", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });

    }

    private void preencherCamposTela(final VagalumeResult vagalumeResult) {
        String lines = "";

//        Log.d("LyricsFragment", "preencherCamposTela() " + vagalumeResult.getArtistName());
        tvNomeMusica.setText(vagalumeResult.getMusicName() + " - " + vagalumeResult.getArtistName());
        formatedLyrics = formatLyrics(vagalumeResult.getMusicText());

        for (int i = 0; i < LINE_NUMBERS_DEFAULT; i++)
            lines += formatedLyrics.get(lyricPivot + i) + "\n";
        tvLetra.setText(lines);

        updateLyrics();
    }

    private void inicializarYoutube(final String videoId) {

        youTubePlayerFragment = new YouTubePlayerSupportFragment();

        youTubePlayerFragment.setRetainInstance(false);
        youTubePlayerFragment.initialize(getString(R.string.DEVELOPER_KEY_YOUTUBE), new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
                if (!wasRestored) {
                    youTubePlayer.cueVideo(videoId);
                    youTubePlayer.setShowFullscreenButton(false);

                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Toast.makeText(getActivity(), "Falha ao carregar o vídeo.", Toast.LENGTH_LONG).show();
                System.err.println(YouTubeInitializationResult.values().toString());
            }
        });

        FragmentManager manager = getChildFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fl_yotube, youTubePlayerFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    public void setVagalumeResult(VagalumeResult vagalumeResult) {
        this.vagalumeResult = vagalumeResult;
    }

    public void setAlreadyLoaded(boolean alreadyLoaded) {
        this.alreadyLoaded = alreadyLoaded;
    }

    private void updateLyrics() {

        updateLyricsThread = new Thread(new Runnable() {

            long now
                    ,
                    then
                    ,
                    deltaTime;
            boolean stream = true;

            public void update() {
                lyricPivot = 0;
                try {
                    while (!Thread.interrupted()) {
                        while (lyricPivot < formatedLyrics.size() - lineNumbers) {
                            then = System.currentTimeMillis();
                            while (stream) {
                                now = System.currentTimeMillis();
                                deltaTime = now - then;

                                if ((deltaTime >= (swapSpeed * 1000 + 1000) && !pauseLyric) || updateLyric) {

                                    if (getActivity() != null) {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    String lines = "";
                                                    for (int i = 0; i < lineNumbers; i++)
                                                        lines += formatedLyrics.get(lyricPivot + i) + "\n";

                                                    tvLetra.setText(lines);
                                                    lyricPivot++;

                                                } catch (Exception e) {
                                                    Log.w("Update Lyrics Thread", e);
                                                }
                                            }
                                        });
                                    }
                                    stream = false;
                                    updateLyric = false;
                                } else {
                                    Thread.sleep(250);
                                }
                            }

                            stream = true;
                        }

                        Thread.sleep(5000);
                        lyricPivot = 0;

                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String lines = "";

                                    PauseLyrics();
                                    for (int i = 0; i < LINE_NUMBERS_DEFAULT; i++)
                                        lines += formatedLyrics.get(lyricPivot + i) + "\n";
                                    tvLetra.setText(lines);
                                }
                            });
                        }
                    }
                } catch (InterruptedException e) {
                    Thread.interrupted();
                }
            }

            @Override
            public void run() {
                update();
            }
        });

        updateLyricsThread.start();
    }

    private ArrayList<String> formatLyrics(String rawLyrics) {
        ArrayList<String> formatedLyrics = new ArrayList<String>();

        Scanner rawScanner = new Scanner(rawLyrics);

        while (rawScanner.hasNextLine())
            formatedLyrics.add(rawScanner.nextLine());

        return formatedLyrics;
    }

    private void PauseLyrics() {
        if (!pauseLyric) {
            pauseLyric = true;
            pauseButton.setText("Começar Transição");
        }
    }

    private void UnpauseLyrics() {
        if (pauseLyric) {
            pauseLyric = false;
            pauseButton.setText("Pausar Letra");
        }
    }

    private void favoriteLyric() {
        if (vagalumeResult != null && !vagalumeResult.isFavorite()) {
            vagalumeResult.setFavorite(true);
            VagalumeDAO.newInstance(getActivity()).addVagalumeResult(vagalumeResult);
//            Toast.makeText(getActivity(), "Letra foi favoritada!", Toast.LENGTH_LONG).show();
        } else if (vagalumeResult != null && vagalumeResult.isFavorite()) {
            vagalumeResult.setFavorite(false);
            VagalumeDAO.newInstance(getActivity()).addVagalumeResult(vagalumeResult);
//            Toast.makeText(getActivity(), "Letra foi desfavoritada!", Toast.LENGTH_LONG).show();
        }
    }
}
