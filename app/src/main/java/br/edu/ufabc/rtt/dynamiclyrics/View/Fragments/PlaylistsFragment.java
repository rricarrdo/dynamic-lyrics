package br.edu.ufabc.rtt.dynamiclyrics.View.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import br.edu.ufabc.rtt.dynamiclyrics.Controller.PlaylistAdapter;
import br.edu.ufabc.rtt.dynamiclyrics.Model.Playlist;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeDAO;
import br.edu.ufabc.rtt.dynamiclyrics.R;
import br.edu.ufabc.rtt.dynamiclyrics.View.MainActivity;

/**
 * Created by Thz on 01/05/2015.
 */
public class PlaylistsFragment extends ListFragment implements InsertPlaylistDialog.DialogDismissal {
    private IComportamentoFragments callback;
    private PlaylistAdapter adapter;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Qaundo o fragment eh associado na activity, podemos recuperar a Interface para fazer a comunicacao
        callback = (IComportamentoFragments) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new PlaylistAdapter(getActivity());
        setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_playlists, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        registerForContextMenu(getListView());
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getActivity().getMenuInflater().inflate(R.menu.menu_list_context_playlists, menu);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        int id = item.getItemId();

        if (id == R.id.action_remove) {
            VagalumeDAO dao = VagalumeDAO.newInstance(getActivity());
            Playlist playlist = (Playlist) getListAdapter().getItem(info.position);

            dao.removePlaylist(playlist);

            adapter = new PlaylistAdapter(getActivity());
            setListAdapter(adapter);
            getListView().invalidateViews();
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();

        adapter = new PlaylistAdapter(getActivity());
        setListAdapter(adapter);
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Playlist playlist = (Playlist) adapter.getItem(position);
        callback.abrirLyricsListFragment(playlist);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_playlist, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        int id = item.getItemId();

        switch (id) {
            case R.id.action_add_playlist:
                DialogFragment insertDialog = new InsertPlaylistDialog();
                insertDialog.setTargetFragment(this, 0);
                insertDialog.show(getActivity().getSupportFragmentManager(), "InsertDialogFragment");

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDialogDismissal(){
        adapter = new PlaylistAdapter(getActivity());
        setListAdapter(adapter);
        getListView().invalidateViews();
    }


}
