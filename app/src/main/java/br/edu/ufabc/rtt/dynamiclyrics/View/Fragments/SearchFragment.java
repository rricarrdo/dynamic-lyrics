package br.edu.ufabc.rtt.dynamiclyrics.View.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import br.edu.ufabc.rtt.dynamiclyrics.Controller.VagalumeResultAdapter;
import br.edu.ufabc.rtt.dynamiclyrics.Model.Utils.NetworkUtils;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeDAO;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeResult;
import br.edu.ufabc.rtt.dynamiclyrics.R;
import br.edu.ufabc.rtt.dynamiclyrics.View.MainActivity;

public class SearchFragment extends Fragment implements View.OnClickListener {
    private IComportamentoFragments callback;
    private ListView lvUltimasBuscas;
    private VagalumeResultAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_search_lyrics, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //Qaundo o fragment eh associado na activity, podemos recuperar a Interface para fazer a comunicacao
        callback = (IComportamentoFragments)activity;
    }

    @Override
    public void onStart() {
        super.onStart();

        // Quando o fragment esta atrelado, e pronto para ser exibido
        // Colocamos o comportamento onClick desse Fragment no botao BUSCAR
        Button btnSearchLyrics = (Button) getActivity().findViewById(R.id.btnBuscar);
        btnSearchLyrics.setOnClickListener(this);

        EditText etNomeMusica = (EditText) getActivity().findViewById(R.id.etNomeMusica);
        etNomeMusica.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    handleSearch();
                    handled = true;
                }
                return handled;
            }
        });

        adapter = new VagalumeResultAdapter(getActivity(), MainActivity.LAST_SEARCHES_MODE, null);
        lvUltimasBuscas = (ListView) getActivity().findViewById(R.id.lvUltimasBuscas);
        lvUltimasBuscas.setAdapter(adapter);

        lvUltimasBuscas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                VagalumeResult result = (VagalumeResult) adapter.getItem(position);
                VagalumeDAO.newInstance(getActivity()).addVagalumeResult(result);
                callback.musicaArmazenada(result);
                adapter.updateContent();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((VagalumeResultAdapter)lvUltimasBuscas.getAdapter()).notifyDataSetChanged();

        EditText etNomeArtista = (EditText) getActivity().findViewById(R.id.etNomeArtista);
        EditText etNomeMusica = (EditText) getActivity().findViewById(R.id.etNomeMusica);
        etNomeArtista.setText("");
        etNomeMusica.setText("");
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btnBuscar:
                handleSearch();
                break;
            default:
                Toast.makeText(v.getContext(), "Botão Desconhecido...", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_search_lyrics, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_favorites:
                callback.abrirLyricsListFragment(null);
                return true;
            case R.id.action_playlists:
                callback.abrirPlaylistsFragment();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void handleSearch(){
        hideKeyboard();
        if(NetworkUtils.isNetworkConnected(getActivity())) {
            callback.musicaBuscada();
        }else{
            Toast.makeText(getActivity(), "Conecte-se à internet para buscar novas músicas", Toast.LENGTH_LONG).show();
        }
    }

    private void hideKeyboard(){
        InputMethodManager inputMethodManager =(InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }
}
