package br.edu.ufabc.rtt.dynamiclyrics.View.Fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import br.edu.ufabc.rtt.dynamiclyrics.R;

/**
 * Created by Thz on 08/04/2015.
 */
public class SettingsFragment extends PreferenceFragment{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
