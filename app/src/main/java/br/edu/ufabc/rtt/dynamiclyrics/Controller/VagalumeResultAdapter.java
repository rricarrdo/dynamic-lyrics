package br.edu.ufabc.rtt.dynamiclyrics.Controller;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.edu.ufabc.rtt.dynamiclyrics.Model.Playlist;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeDAO;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeResult;
import br.edu.ufabc.rtt.dynamiclyrics.R;
import br.edu.ufabc.rtt.dynamiclyrics.View.MainActivity;

/**
 * Created by Thz on 17/04/2015.
 */
public class VagalumeResultAdapter extends BaseAdapter {
    private ArrayList<VagalumeResult> vagalumeResults;

    private LayoutInflater inflater;
    private Context context;

    public VagalumeResultAdapter(Context context, int mode, Playlist playlist){
        this.context = context;

        if(mode == MainActivity.LAST_SEARCHES_MODE)
            vagalumeResults = VagalumeDAO.newInstance(context).getMostRecents();
        else if(mode == MainActivity.FAVORITES_MODE)
            vagalumeResults = VagalumeDAO.newInstance(context).getFavorites();
        else if(mode == MainActivity.PLAYLIST_MODE && playlist != null)
            vagalumeResults = VagalumeDAO.newInstance(context).getVagalumeResultsByPlaylist(playlist);
        else
            vagalumeResults = VagalumeDAO.newInstance(context).getMostRecents(); // apenas para garantir que alguma coisa seja carregada (estado não esperado)

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return vagalumeResults.size();
    }

    @Override
    public Object getItem(int position) {
        return vagalumeResults.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vagalumeResultRow = convertView;
        VagalumeResultViewHolder holder = null;

        if(vagalumeResultRow == null){
            vagalumeResultRow = inflater.inflate(R.layout.most_recent_row, parent, false);
            holder = new VagalumeResultViewHolder(vagalumeResultRow);
            vagalumeResultRow.setTag(holder);
        }else{
            holder = (VagalumeResultViewHolder)vagalumeResultRow.getTag();
        }

        VagalumeResult vagalumeResult = vagalumeResults.get(position);

        holder.musicNameTextView.setText(vagalumeResult.getMusicName());
        holder.artistNameTextView.setText(vagalumeResult.getArtistName());

        return vagalumeResultRow;
    }

    public void updateContent(){
        vagalumeResults.clear();
        vagalumeResults.addAll(VagalumeDAO.newInstance(context).getMostRecents());
//        Log.d("ADAPTER", "UpdateContent " + getCount());
    }

    private class VagalumeResultViewHolder{
        TextView musicNameTextView;
        TextView artistNameTextView;

        VagalumeResultViewHolder(View view) {
            musicNameTextView = (TextView)view.findViewById(R.id.tvMusicName);
            artistNameTextView = (TextView)view.findViewById(R.id.tvArtistName);
        }
    }
}
