package br.edu.ufabc.rtt.dynamiclyrics.View;


import android.app.Activity;
import android.os.Bundle;

import br.edu.ufabc.rtt.dynamiclyrics.View.Fragments.SettingsFragment;

/**
 * Created by Thz on 08/04/2015.
 */
public class SettingsActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}
