package br.edu.ufabc.rtt.dynamiclyrics.View;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import br.edu.ufabc.rtt.dynamiclyrics.Controller.Services.VagalumeFetcher_Service;
import br.edu.ufabc.rtt.dynamiclyrics.Controller.VagalumeResultAdapter;
import br.edu.ufabc.rtt.dynamiclyrics.Model.Playlist;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeDAO;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeResult;
import br.edu.ufabc.rtt.dynamiclyrics.R;
import br.edu.ufabc.rtt.dynamiclyrics.View.Fragments.IComportamentoFragments;
import br.edu.ufabc.rtt.dynamiclyrics.View.Fragments.LyricsFragment;
import br.edu.ufabc.rtt.dynamiclyrics.View.Fragments.LyricsListFragment;
import br.edu.ufabc.rtt.dynamiclyrics.View.Fragments.PlaylistsFragment;
import br.edu.ufabc.rtt.dynamiclyrics.View.Fragments.SearchFragment;


public class MainActivity extends ActionBarActivity implements IComportamentoFragments {
    public static final int LAST_SEARCHES_MODE = 1;
    public static final int FAVORITES_MODE = 2;
    public static final int PLAYLIST_MODE = 4;
    public static final String LIST_MODE = "br.edu.ufabc.rtt.dynamiclyrics.LIST_MODE";
    boolean recarregar = false;
    Menu menu;
    private VagalumeResult vagalumeResult;
    private DownloadReceiver receiver;
    private String nomeArtista;
    private String nomeMusica;

    @Override
    protected void onResume() {
        registerBroadcast();
        super.onResume();
    }

    @Override
    protected void onPause() {
        unRegisterBroadcast();
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        setContentView(R.layout.activity_main);
        //Faz o controle para não recarregar os fragments a toa.
        if (savedInstanceState == null || recarregar) {
            //Tenta buscar o fragment_unico -> Layout Portrait
            View fragmentUnico = findViewById(R.id.fragment_unico);

            // Se a tela esta em  modo Retrato
            if (fragmentUnico != null) {
                //Adiciona o fragment de Busca de Letras
                SearchFragment searchFragment = new SearchFragment();
                searchFragment.setArguments(savedInstanceState);

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_unico, searchFragment);
                transaction.commit();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void musicaBuscada() {
        EditText etNomeArtista = (EditText) findViewById(R.id.etNomeArtista);
        EditText etNomeMusica = (EditText) findViewById(R.id.etNomeMusica);

        nomeArtista = etNomeArtista.getText().toString();
        nomeMusica = etNomeMusica.getText().toString();

        boolean valido = true;
        if (nomeMusica.equals("")) {
            etNomeMusica.setError("Digite o nome da Música");
            valido = false;
        }
        if (nomeArtista.equals("")) {
            etNomeArtista.setError("Digite o nome do Artista");
            valido = false;
        }
        Log.d("MainActivity", "musicaBuscada()");
        //Se os campos estiverem corretamente preenchidos
        if (valido) {
            buscarLetra();
        }
    }

    private void buscarLetra() {

        Intent intent = new Intent(this, VagalumeFetcher_Service.class);
        intent.putExtra(VagalumeFetcher_Service.PARAMS_NOME_ARTISTA, nomeArtista);
        intent.putExtra(VagalumeFetcher_Service.PARAMS_NOME_MUSICA, nomeMusica);

        startService(intent);

    }

    @Override
    public void musicaArmazenada(VagalumeResult vagalumeResult) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        LyricsFragment lyricsFragment = new LyricsFragment();
        lyricsFragment.setVagalumeResult(vagalumeResult);
        lyricsFragment.setAlreadyLoaded(true);

        Log.d("MainActivity", "musicaArmazenada()");
        //Tenta buscar o fragment_unico -> Layout Portrait
        View fragmentUnico = findViewById(R.id.fragment_unico);

        if (fragmentUnico != null) {
            transaction.replace(R.id.fragment_unico, lyricsFragment);
        } else {
            transaction.replace(R.id.fragment_direita, lyricsFragment);
        }
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void abrirLyricsListFragment(Playlist playlist) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        LyricsListFragment lyricsListFragment = new LyricsListFragment();

        Bundle args = new Bundle();

        if (playlist == null)
            args.putInt(LIST_MODE, FAVORITES_MODE);
        else {
            args.putInt(LIST_MODE, PLAYLIST_MODE);
            args.putSerializable("Playlist", playlist);
        }

        lyricsListFragment.setArguments(args);

        View fragmentUnico = findViewById(R.id.fragment_unico);

        if (fragmentUnico != null) {
            transaction.replace(R.id.fragment_unico, lyricsListFragment);
        } else {
            transaction.replace(R.id.fragment_direita, lyricsListFragment);
        }
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void abrirPlaylistsFragment() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        PlaylistsFragment playlistsFragment = new PlaylistsFragment();

        View fragmentUnico = findViewById(R.id.fragment_unico);

        if (fragmentUnico != null) {
            transaction.replace(R.id.fragment_unico, playlistsFragment);
        } else {
            transaction.replace(R.id.fragment_direita, playlistsFragment);
        }
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    private void registerBroadcast() {
        IntentFilter downloadCompleteFilter = new IntentFilter(VagalumeFetcher_Service.BROADCAST_ACTION);

        receiver = new DownloadReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, downloadCompleteFilter);
    }

    private void unRegisterBroadcast() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }


    private class DownloadReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(VagalumeFetcher_Service.BROADCAST_ACTION)) {
                vagalumeResult = (VagalumeResult) intent.getSerializableExtra(VagalumeFetcher_Service.PARAM_VAGALUME_RESULT);

                if (vagalumeResult != null) {
                    VagalumeDAO.newInstance(MainActivity.this).addVagalumeResult(vagalumeResult);

                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();

                    LyricsFragment lyricsFragment = new LyricsFragment();
                    lyricsFragment.setVagalumeResult(vagalumeResult);

                    View fragmentUnico = findViewById(R.id.fragment_unico);
                    //Tenta buscar o fragment_unico -> Layout Portrait
                    if (fragmentUnico != null) {
                        transaction.replace(R.id.fragment_unico, lyricsFragment);
                    } else {
                        transaction.replace(R.id.fragment_direita, lyricsFragment);
                    }
                    transaction.addToBackStack(null);
                    transaction.commit();
                    ListView lvUltimasBuscadas = (ListView) findViewById(R.id.lvUltimasBuscas);
                    lvUltimasBuscadas.setAdapter(new VagalumeResultAdapter(getBaseContext(), MainActivity.LAST_SEARCHES_MODE, null));
                    lvUltimasBuscadas.invalidateViews();
                } else {
                    Toast.makeText(MainActivity.this, "Música não encontrada... :/", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
