package br.edu.ufabc.rtt.dynamiclyrics.Model;

import android.content.Context;

import java.util.ArrayList;

import br.edu.ufabc.rtt.dynamiclyrics.Model.Utils.VagalumeSQLiteHelper;

/**
 * Created by Ricardo on 06/04/2015.
 */
public class VagalumeDAO {
    private static VagalumeDAO dao;
    public ArrayList<VagalumeResult> vagalumeResults;
    private Context context;

    private VagalumeSQLiteHelper dbHelper;

    protected VagalumeDAO(Context c) {
        dbHelper = new VagalumeSQLiteHelper(c);
        vagalumeResults = new ArrayList<>();
        context = c;
    }

    public static VagalumeDAO newInstance(Context c) {
        if (dao == null) {
            dao = new VagalumeDAO(c);
        } else
            dao.context = c;

        return dao;
    }

    public int size() {
        return vagalumeResults.size();
    }

    public void addVagalumeResult(VagalumeResult vagalumeResult) {
        dbHelper.insertVagalumeResult(vagalumeResult);
    }

    public void addPlaylist(Playlist playlist){
        dbHelper.insertPlaylist(playlist);
    }

    public void removePlaylist(Playlist playlist){
        dbHelper.removePlaylist(playlist);
    }

    public void addVagalumeResultPlaylist(VagalumeResult result, Playlist playlist){
        dbHelper.insertVagalumeResultPlaylist(result, playlist);
    }

    public void removeVagalumeResultPlaylist(VagalumeResult result, Playlist playlist){
        dbHelper.removeVagalumeResultPlaylist(result, playlist);
    }

    public void updateAccessDate(VagalumeResult vagalumeResult) {
        dbHelper.updateAccessTime(vagalumeResult);
    }


    public VagalumeResult buscarMusica(String idMusica) {
        return dbHelper.getMusic(idMusica);
    }

    public ArrayList<VagalumeResult> getMostRecents() {
        return dbHelper.getMostRecents();
    }

    public ArrayList<VagalumeResult> getFavorites() {
        return dbHelper.getFavorites();
    }

    public VagalumeResult getMusicOnPlaylist(Playlist playlist, VagalumeResult vagalumeResult){
        return dbHelper.getMusicOnPlaylist(playlist, vagalumeResult);
    }

    public ArrayList<VagalumeResult> getVagalumeResultsByPlaylist(Playlist playlist) { return dbHelper.getVagalumeResultsByPlaylist(playlist); }

    public ArrayList<Playlist> getAllPlaylists() { return dbHelper.getAllPlaylists(); }

    public Playlist getPlaylistByName(String name){ return dbHelper.getPlaylistByName(name);}

    public int getMusicsCountByPlaylist(Playlist playlist){ return dbHelper.getMusicsCountByPlaylist(playlist); }
}
