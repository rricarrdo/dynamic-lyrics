package br.edu.ufabc.rtt.dynamiclyrics.View.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

import java.util.ArrayList;

import br.edu.ufabc.rtt.dynamiclyrics.Model.Playlist;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeDAO;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeResult;
import br.edu.ufabc.rtt.dynamiclyrics.R;

/**
 * Created by Thz on 01/05/2015.
 */
public class ShowPlaylistsDialog extends DialogFragment {
    private ArrayList<Playlist> playlists;
    private CharSequence[] playlistNames;
    private VagalumeResult vagalumeResult;

    public ShowPlaylistsDialog(){
        this.playlists = VagalumeDAO.newInstance(getActivity()).getAllPlaylists();
        this.playlistNames = new CharSequence[playlists.size()];

        for(int i = 0; i < playlists.size(); i++){
            playlistNames[i] = playlists.get(i).getName();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.vagalumeResult = (VagalumeResult) getArguments().getSerializable("VagalumeResult");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder .setTitle(R.string.add_to_playlist)
                .setItems(playlistNames, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (VagalumeDAO.newInstance(getActivity()).getMusicOnPlaylist(playlists.get(which), vagalumeResult) == null) {
                            VagalumeDAO.newInstance(getActivity()).addVagalumeResultPlaylist(vagalumeResult, playlists.get(which));
                        } else {
                            Toast.makeText(getActivity(), "Música já foi adicionada!", Toast.LENGTH_LONG).show();
                        }
                    }
                })

                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ShowPlaylistsDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
}
