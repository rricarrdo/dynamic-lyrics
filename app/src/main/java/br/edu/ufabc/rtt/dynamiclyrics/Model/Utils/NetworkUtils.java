package br.edu.ufabc.rtt.dynamiclyrics.Model.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Ricardo on 08/04/2015.
 */
public class NetworkUtils {


    public static boolean isNetworkConnected(Context context) {
        boolean status = false;
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected())
            status = true;

        return status;
    }
}
