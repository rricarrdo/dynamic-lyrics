package br.edu.ufabc.rtt.dynamiclyrics.View.Fragments;

import br.edu.ufabc.rtt.dynamiclyrics.Model.Playlist;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeResult;

/**
 * Created by Ricardo on 08/04/2015.
 */
public interface IComportamentoFragments {

    void musicaBuscada();
    void musicaArmazenada(VagalumeResult vagalumeResult);
    void abrirLyricsListFragment(Playlist playlist);
    void abrirPlaylistsFragment();
}
