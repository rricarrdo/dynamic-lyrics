package br.edu.ufabc.rtt.dynamiclyrics.Model.Utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeResult;

/**
 * Created by Ricardo on 06/04/2015.
 */
public class Vagalume_Serializer {

    private static final String LOGTAG = Vagalume_Serializer.class.getSimpleName();

    private static final String TYPE = "type";
    private static final String SONG_NOTFOUND = "song_notfound";

    private static final String ARTISTA = "art";
    private static final String ART_ID = "id";
    private static final String ART_NAME = "name";
    private static final String ART_URL = "url";

    private static final String MUSICA = "mus";
    private static final String MUS_ID = "id";
    private static final String MUS_NAME = "name";
    private static final String MUS_LANG = "lang";
    private static final String MUS_TEXT = "text";
    private static final String MUS_YOUTUBE_ID = "ytid";

    private static final String MUS_TRANSLATE = "translate";

    private static final String MUS_TRANSLATE_ID = "id";
    private static final String MUS_TRANSLATE_LANG = "lang";
    private static final String MUS_TRANSLATE_URL = "url";
    private static final String MUS_TRANSLATE_TEXT = "text";


    public static VagalumeResult deserialize(String jsonStr) {
        VagalumeResult vagalumeResult = new VagalumeResult();
        JSONObject root;

        try {
            JSONObject rootArtista;
            JSONArray rootMusica;
            JSONObject objectMusica;
            JSONArray rootTranslate;
            root = new JSONObject(jsonStr);

            vagalumeResult.setType(root.getString(TYPE));
            //Se nao encontar a letra da música retorna null
            if(vagalumeResult.getType().equals(SONG_NOTFOUND)){
                return null;
            }

            rootArtista = root.getJSONObject(ARTISTA);
            rootMusica = root.getJSONArray(MUSICA);

            vagalumeResult.setArtistId(rootArtista.getString(ART_ID));
            vagalumeResult.setArtistName(rootArtista.getString(ART_NAME));
            vagalumeResult.setArtistUrl(rootArtista.getString(ART_URL));

            objectMusica = rootMusica.getJSONObject(0);
            //Busca apenas a primeira posição do array que eh a musica correta
            vagalumeResult.setMusicId(objectMusica.getString(MUS_ID));
            vagalumeResult.setMusicName(objectMusica.getString(MUS_NAME));
            vagalumeResult.setMusicLang(objectMusica.getString(MUS_LANG));
            vagalumeResult.setMusicText(objectMusica.getString(MUS_TEXT));

            if (!objectMusica.isNull(MUS_YOUTUBE_ID)) {
                vagalumeResult.setYoutubeID(objectMusica.getString(MUS_YOUTUBE_ID));
            }

            if (!objectMusica.isNull(MUS_TRANSLATE)) {
                rootTranslate = objectMusica.getJSONArray(MUS_TRANSLATE);

                vagalumeResult.setTranslateId(rootTranslate.getJSONObject(0).getString(MUS_TRANSLATE_ID));
                vagalumeResult.setTranslateLang(rootTranslate.getJSONObject(0).getString(MUS_TRANSLATE_LANG));
                vagalumeResult.setTranslateUrl(rootTranslate.getJSONObject(0).getString(MUS_TRANSLATE_URL));
                vagalumeResult.setTranslateText(rootTranslate.getJSONObject(0).getString(MUS_TRANSLATE_TEXT));
            }
        } catch (JSONException e) {
            Log.e(LOGTAG, "Failed to parse JSON", e);
            vagalumeResult = null;
        }

        return vagalumeResult;
    }

}
