package br.edu.ufabc.rtt.dynamiclyrics.Controller;

import android.net.Uri;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import br.edu.ufabc.rtt.dynamiclyrics.Model.Utils.Vagalume_Serializer;
import br.edu.ufabc.rtt.dynamiclyrics.Model.VagalumeResult;

/**
 * Created by Ricardo on 06/04/2015.
 */
public class Vagalume_Fetcher {

    private static final String ENDPOINT = "http://api.vagalume.com.br/search.php";

    private static final String ART_K = "art";
    private static final String MUS_K = "mus";
    private static final String EXTRA_K = "extra";

    private static final String EXTRA_V = "ytid";

    private static final String LOGTAG = Vagalume_Fetcher.class.getSimpleName();
    private static final int BUFFER_SIZE = 1024;


    public VagalumeResult fetch(String nomeArtista, String nomeMusica){
        VagalumeResult vagalumeResult = null;
        URL url;
        String urlStr = null;
        HttpURLConnection conn = null;
        int responseCode;

        try{
            urlStr = Uri.parse(ENDPOINT).buildUpon()
                    .appendQueryParameter(ART_K, nomeArtista)
                    .appendQueryParameter(MUS_K, nomeMusica)
                    .appendQueryParameter(EXTRA_K, EXTRA_V)
                    .build().toString();


            url = new URL(urlStr);
            conn = (HttpURLConnection) url.openConnection();
            responseCode = conn.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK){
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                InputStream inputStream = conn.getInputStream();
                int len;
                byte[] buffer = new byte[BUFFER_SIZE];

                while((len = inputStream.read(buffer)) > 0){
                    outputStream.write(buffer, 0, len);
                }

                outputStream.flush();
                outputStream.close();
                inputStream.close();

                // deserialize JSON
                String jsonStr = new String(outputStream.toByteArray());
                vagalumeResult = Vagalume_Serializer.deserialize(jsonStr);
            }

        }catch (MalformedURLException e) {
            Log.e(LOGTAG, "Malformed URL", e);
        }catch (IOException e) {
            Log.e(LOGTAG, "Failed to open URL", e);
        }

        return vagalumeResult;
    }
}
